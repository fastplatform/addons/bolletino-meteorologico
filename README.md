# FaST Platform / addons / bolletino-meteorologico


Additional module that implements the meteorological services of Piemonte (Italy) used for weather observations and forecasts.

This repository includes the [source code](services) and [a descriptive orchestration configuration](manifests) of the entire package.

## Services

### Meteorology

- [meteorology/weather](services/meteorology/weather)
