# Date & time
python-dateutil==2.8.2
pytz==2021.1

# GraphQL
graphene==3.0b7
starlette_graphene3==0.5.1

# GIS
Fiona==1.8.20
pyproj==3.1.0
shapely==1.7.1

# HTTP client
requests>=2.23.0

# Internal
# TODO: remove this deprecated library
#git+https://gitlab.com/fastplatform/pypi/fastplatform.git@7e4e067ac174698161a0d29fdf7e152d0c659c9b#egg=fastplatform_graphql&subdirectory=fastplatform-graphql

# OpenTelemetry
asgiref~=3.0
opentelemetry-exporter-zipkin==1.6.2
opentelemetry-instrumentation-fastapi==0.25b2
opentelemetry-propagator-b3==1.6.2

# Server
fastapi==0.66.0
geojson-pydantic==0.3.0
pydantic==1.8.2
setuptools==50.3.0 # get pkg_resources package for pydantic
uvicorn>=0.14.0

#XML
xmltodict==0.12.0
