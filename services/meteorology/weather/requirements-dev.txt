# Linting
pylint==2.9.6
pylint-plugin-utils==0.6

# Code formatting
black==21.7b0
