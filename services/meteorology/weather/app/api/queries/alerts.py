import json

import graphene
from graphql import GraphQLError
from shapely.geometry import shape

from fastplatform_graphql.types.weather import Alert

from app.lib.gis import Projection
from app.lib import arpa


class Alerts:

    # Describe capabilities
    alerts_implemented = graphene.Boolean(default_value=True)

    # Node and resolver for alerts
    alerts = graphene.List(
        Alert, geometry=graphene.Argument(graphene.String, required=True)
    )

    def resolve_alerts(self, info, geometry):
        """Resolver for `alerts` node

        Arguments:
            info {object} -- GraphQL context
            geometry {str} -- Geometry as a GeoJSON string on ETRS89 datum

        Returns:
            list -- List of Alerts
        """
        if not info.context["request"].headers["X-Hasura-User-Id"]:
            return None

        geometry = shape(json.loads(geometry))
        alerts = arpa.bolletino_meteorologico_client.get_alerts_for_geometry(geometry)

        return [Alert(**a) for a in alerts]
