import graphene


from app.api.queries.alerts import Alerts
from app.api.queries.forecasts import Forecasts
from app.api.queries.observations import Observations


class Weather(Alerts, Forecasts, Observations, graphene.ObjectType):
    class Meta:
        name = "weather"