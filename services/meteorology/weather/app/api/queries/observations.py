import graphene
from graphql import GraphQLError

from fastplatform_graphql.types.weather import Sample, IntervalType


class Observations:

    # Describe capabilities
    observations_implemented = graphene.Boolean(default_value=False)
    observations_interval_types = graphene.List(IntervalType)

    def resolve_observations_interval_types(self, info):
        return None

    # Node and resolver for observations
    observations = graphene.List(
        Sample,
        geometry=graphene.Argument(graphene.String, required=True),
        interval_type=graphene.Argument(IntervalType, required=True),
        date_from=graphene.Argument(graphene.Date, required=False),
        date_to=graphene.Argument(graphene.Date, required=False),
    )

    def resolve_observations(self, info, geometry, interval_type, date_from, date_to):
        """Resolver for `observations` node

        Arguments:
            info {object} -- GraphQL context
            geometry {str} -- Geometry as a GeoJSON string
            date_from {date} -- [description]
            date_to {str} -- [description]

        Returns:
            list -- List of weather Samples
        """
        if not info.context["request"].headers["X-Hasura-User-Id"]:
            return None

        # Currently no access to previous observations from the ARPA weather service
        return []
