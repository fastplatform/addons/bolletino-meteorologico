import csv
import logging
import pytz
import requests
import xmltodict
import json
import dateutil.parser
import fiona

from datetime import datetime, timedelta
from shapely.geometry import shape
from shapely.strtree import STRtree

from app.settings import config
from app.lib.gis import Projection

TZ = pytz.utc

logger = logging.getLogger(__name__)


class BolletinoMeteorologicoClient:
    """A client class to deal with connections and caching of the ARPA
    weather XML endpoints
    """

    def __init__(self):
        self.forecasts_url = config.ARPA_XML_FORECASTS_URL
        self.forecasts_max_age = timedelta(seconds=config.ARPA_XML_FORECASTS_MAX_AGE)
        self.alerts_url = config.ARPA_XML_ALERTS_URL
        self.alerts_max_age = timedelta(seconds=config.ARPA_XML_ALERTS_MAX_AGE)

        # Mapping from ARPA <skyCondition> codes to FaST codes/icons
        self.icons_mapping = {}
        arpa_codes_file = config.API_DIR / "data/arpa-codes.csv"
        with open(arpa_codes_file, "r") as f:
            reader = csv.DictReader(f)
            for row in reader:
                self.icons_mapping[row["ARPA_SKY_CONDITION_CODE"]] = row["FAST_CODE"]

        # Load provinces layer (expected in WGS84/UTM32N)
        layer_file = config.API_DIR / "data/shapefiles/Ambiti_Amministrativi-Province.shp"
        data = fiona.open(layer_file, "r")
        
        # Read and reproject to ETRS89
        shapes = [Projection.wsg84_utm32n_to_etrs89(shape(f["geometry"])) for f in data]
        
        # Index
        self.provinces_tree = STRtree(shapes)
        self.provinces_index = dict((id(s), f) for s, f in zip(shapes, data))

        # Load alert zones layer (expected in WGS84/UTM32N)
        # https://www.arpa.piemonte.it/rischinaturali/approfondimenti/pericoli-meteo/sistema_regionale_di_allertamento/Zone-di-allerta.html
        layer_file = config.API_DIR / "data/shapefiles/ZA_Piemonte.shp"
        data = fiona.open(layer_file, "r")

        # Read and reproject to ETRS89
        shapes = [Projection.wsg84_utm32n_to_etrs89(shape(f["geometry"])) for f in data]

        # Index
        self.alert_zones_tree = STRtree(shapes)
        self.alert_zones_index = dict((id(s), f) for s, f in zip(shapes, data))

        # Prepare caches
        self.forecasts_cache = {"data": None, "timestamp": datetime(2000, 1, 1)}
        self.alerts_cache = {"data": None, "timestamp": datetime(2000, 1, 1)}

        self.origin = {"name": None, "url": None}

    def set_origin(self, credits):
        if credits is None:
            return
        self.origin["name"] = credits.get("source", None)
        self.origin["url"] = credits.get("link", {}).get("@url", None)
        self.origin["language"] = "it-it"

    def fetch_forecast(self):
        """Fetch forecast for all of Piemonte and cache it in memory as a dict"""
        response = requests.get(self.forecasts_url)
        data = xmltodict.parse(response.content)
        weather_data = data.get("weatherData", {})
        if weather_data is not None:
            for forecast in weather_data["forecast"]:
                forecast["districts_dict"] = {
                    district["districtName"]: district
                    for district in forecast["areas"]["districts"]["district"]
                }

        self.set_origin(weather_data.get("credits", None))

        self.forecasts_cache = {"data": weather_data, "timestamp": datetime.now()}

    def fetch_alerts(self):
        """Fetch alerts for Piemonte and cache them in memory as a dict"""
        response = requests.get(self.alerts_url)
        data = xmltodict.parse(response.content)
        alert = data.get("alert", {})

        self.alerts_cache = {"data": alert, "timestamp": datetime.now()}

    def get_province_from_geometry(self, geometry):
        """Find the polygon with the biggest intersection with this geometry

        Arguments:
            geometry {dict} -- A shapely geometry on ETRS89

        Returns:
            dict -- The feature with the biggest intersection
        """

        # Memory IDs of shapes that intersect the geometry
        shape_ids = [id(s) for s in self.provinces_tree.query(geometry)]

        if not shape_ids:
            return None

        # Features corresponding to these memory IDs, as per the index
        features = [self.provinces_index[shape_id] for shape_id in shape_ids]

        # Find max intersection
        intersection_areas = [
            shape(f["geometry"]).intersection(geometry).area for f in features
        ]
        index_intersection_area_max = max(
            range(len(intersection_areas)), key=lambda i: intersection_areas[i]
        )

        return features[index_intersection_area_max]

    def get_alert_zones_from_geometry(self, geometry):
        """Find the alert zones intersecting with this geometry

        Arguments:
            geometry {dict} -- A shapely geometry on ETRS89

        Returns:
            dict -- The features with non-empty intersection
        """
        # Memory IDs of shapes that intersect the geometry
        shape_ids = [id(s) for s in self.alert_zones_tree.query(geometry)]

        # Features corresponding to these memory IDs, as per the index
        return [self.alert_zones_index[shape_id] for shape_id in shape_ids]

    def get_forecast_for_geometry(self, geometry):
        """Retrieve the forecast for a geometry

        Arguments:
            geometry {dict} -- A shapely geometry on ETRS89

        Returns:
            [Sample] -- List of forecast weather samples
        """
        if datetime.now() > (
            self.forecasts_cache["timestamp"] + self.forecasts_max_age
        ):
            logging.info("Cache is too old for forecasts, fetching again.")
            self.fetch_forecast()

        weather_data = self.forecasts_cache["data"]
        if weather_data is None:
            logger.warning("No weather data received from API")
            return []

        province = self.get_province_from_geometry(geometry)

        if province is None:
            logger.warning("Geometry does not intersect any province")
            return []

        samples = []

        for forecast in weather_data["forecast"]:

            valid_from = dateutil.parser.parse(forecast["forecastDate"]["initDate"])
            valid_to = dateutil.parser.parse(forecast["forecastDate"]["endDate"])

            district = forecast["districts_dict"].get(
                province["properties"]["nome"], None
            )
            if district is None:
                logger.warning(
                    f'District {province["properties"]["nome"]} not found in weather forecast.'
                )
                continue

            sky_condition = district.get("skyCondition", {}).get("code", None)
            icon = self.icons_mapping.get(sky_condition, None)

            # Build location __geo_interface__
            geometry = shape(province["geometry"])
            geometry = Projection.wsg84_utm32n_to_etrs89(geometry)
            geometry = geometry.__geo_interface__
            geometry.update(
                {
                    "crs": {
                        "type": "name",
                        "properties": {"name": f"EPSG:{config.EPSG_SRID_ETRS89}"},
                    }
                }
            )

            location = {
                "id": province["properties"]["nome"],
                "name": province["properties"]["nome"],
                "geometry": geometry,
            }

            # Build result sample
            sample = {
                "id": f'{province["properties"]["nome"]}|{forecast["forecastDate"]["initDate"]}|{forecast["forecastDate"]["endDate"]}',
                "location": location,
                "origin": self.origin,
                "fetched_at": self.forecasts_cache["timestamp"],
                "valid_from": valid_from,
                "valid_to": valid_to,
                "sample_type": "forecast",
                "interval_type": "day",
                "computed_at": datetime.now(),
                "icon": icon,
                "summary": weather_data.get("synoptic", {}).get("text", None),
            }
            if "maximumTemperature" in district:
                sample["temperature_max"] = float(
                    district["maximumTemperature"]["value"]
                )
            if "minimumTemperature" in district:
                sample["temperature_min"] = float(
                    district["minimumTemperature"]["value"]
                )

            samples += [sample]

        return samples

    def get_alerts_for_geometry(self, geometry):
        """Retrieve the alerts for a geometry

        Arguments:
            geometry {dict} -- A shapely geometry on ETRS89

        Returns:
            [Alert] -- List of alerts
        """
        if datetime.now() > self.alerts_cache["timestamp"] + self.alerts_max_age:
            logging.info("Cache is too old for alerts, fetching again.")
            self.fetch_alerts()
        alerts = self.alerts_cache["data"]

        alert_zones = self.get_alert_zones_from_geometry(geometry)
        alert_zones = {az["properties"]["Cod_DPC"]: az for az in alert_zones}

        results = []

        for alert in alerts["info"]:
            # For each alert, check if it is in the alert zone(s) of the geometry
            alert_area = alert.get("area", {}).get("areaDesc", None)
            alert_zone = alert_zones.get(alert_area, None)
            if alert_zone is None:
                continue

            # Build location, this is the alert zone
            location = {
                "id": alert_zone["properties"]["Cod_DPC"],
                "name": alert_zone["properties"]["Nome_zona"],
                "authority": alert_zone["properties"]["Regione"],
                "authority_id": alert_zone["properties"]["Cod_zona"],
            }

            geometry = shape(alert_zone["geometry"])
            geometry = Projection.wsg84_utm32n_to_etrs89(geometry)
            geometry = geometry.__geo_interface__
            geometry.update(
                {
                    "crs": {
                        "type": "name",
                        "properties": {"name": f"EPSG:{config.EPSG_SRID_ETRS89}"},
                    }
                }
            )
            location["geometry"] = geometry

            summary = None
            description = []
            for parameter in alert["parameter"]:
                if parameter.get("valueName") == "EFFETTI SUL TERRITORIO":
                    summary = parameter.get("value", None)
                    if summary == "-":
                        summary = None
                else:
                    description += [
                        parameter.get("valueName") + ": " + parameter.get("value", None)
                    ]
            description = "\n".join(description)

            results += [
                {
                    "id": f'{alerts["identifier"]}|{hash(json.dumps(alert))}',
                    "sender": alerts["sender"],
                    "issued_at": dateutil.parser.parse(alerts["sent"]),
                    "title": alerts["note"],
                    "summmary": summary,
                    "description": description,
                    "url": alert["instruction"],
                    "category": alert["category"].lower(),
                    "urgency": alert["urgency"].lower(),
                    "severity": alert["severity"].lower(),
                    "certainty": alert["certainty"].lower(),
                    "location": location,
                    "valid_from": dateutil.parser.parse(alert["onset"]),
                    "valid_to": dateutil.parser.parse(alert["expires"]),
                }
            ]

        return results


bolletino_meteorologico_client = BolletinoMeteorologicoClient()
