# meteorology/weather

```meteorology/weather``` is a [FastAPI](https://fastapi.tiangolo.com/) service that retrieves climatic data from the [Agenzia Regionale Per la Protezzione Ambientale (ARPA)](http://www.arpa.piemonte.it/) of Piemonte (Italy).

It exposes 3 types of objects through a single GraphQL endpoint:
- **weather observations (past data)**: *currently not implemented as not exposed by ARPA, will always return an empty array*
- **weather forecasts**: fetched from the [ARPA Bolletino Meteorologico XML endpoint ](http://www.arpa.piemonte.it/news/il-bollettino-meteorologico-in-formato-xml-xtensible-markup-language)
- **weather alerts**: fetched from the [ARPA Bollettino di allerta meteoidrologica XML endpoint](http://www.arpa.piemonte.it/news/da-oggi-ancora-piu-facile-la-divulgazione-delle-allerte-meteoidrologiche)

This service therefore exposes a GraphQL ontology that can be natively federated at a higher level into another GraphQL ontology. It instantly enables the modular aspect of Ilmateenistus's custom logic. The exposed GraphQL schema is compliant with the [FaST Weather GraphQL ontology](https://gitlab.com/fastplatform/pypi/fastplatform/-/blob/master/fastplatform-graphql/fastplatform_graphql/types/weather.py).

The service embarks a shapefile with all Piemonte provinces and [Alert Zones](https://www.arpa.piemonte.it/rischinaturali/approfondimenti/pericoli-meteo/sistema_regionale_di_allertamento/Zone-di-allerta.html), which are loaded in memory and geographically indexed when the service starts.

## Dependencies

The service builds upon the ARPA XML endpoints and some GIS information in shapefile format (provinces, alert zones).

## Prerequisites

- Python 3.7+ and [`virtualenv`](https://virtualenv.pypa.io/en/latest/)

## Environment variables

- `ARPA_XML_FORECASTS_URL`: URL of the ARPA XML forecast endpoint (defaults to `http://www.arpa.piemonte.it/export/meteoxml/meteo.xml`)
- `ARPA_XML_FORECASTS_MAX_AGE`: caching duration of the XML forecasts (defaults to `60` seconds)
- `ARPA_XML_ALERTS_URL`: URL of the ARPA XML alerts endpoint (defaults to `http://www.arpa.piemonte.it/export/xmlcap/allerta.xml`)
- `ARPA_XML_ALERTS_MAX_AGE`:  caching duration of the XML alerts (default to `60` seconds)
- `LOG_LEVEL`: logging level (trace, debug, info, warn, error, fatal)


## Development Setup

Create a Python virtualenv and activate it:
```
virtualenv .venv
source .venv/bin/activate
```

Install Python dependencies:
```
pip install -r requirements.txt

# Optionally install dev packages
pip install -r requirements-dev.txt
```

If needed start monitoring OpenTelemetry traces
```
make start-tracing
```
Jaeger UI will be available at [http://localhost:16686](http://localhost:16686)

Start the service:
```
make start
```

The API server is now started and available at [http://localhost:7777/graphql](http://localhost:7777/graphql), with live reload enabled.

**Server can be also started with Bazel** (no need to activate a Python virtualenv):
```bash
$ make bazel-run 
```

## Sample 

A sample (full) GraphQL query is included in the [tests](tests) folder.